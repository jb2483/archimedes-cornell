# Archimedes-cornell

Archimedes-cornell aims to study photoexcited electrons using the Montecarlo method. In addition to the original code archimedes-nitride, it has capability to study spin polarization of electrons.

# Installation
Tested under Ubuntu/Debian
```bash
aclocal
autoconf
automake --add-missing
./configure
make
sudo make install
```

# Credits
Starter code: archimedes nitride (https://github.com/jmarini/archimedes-nitrides)
